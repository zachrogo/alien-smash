﻿using UnityEngine;
using System.Collections;

public class Safety : MonoBehaviour
{
    private GameManager _gameManager;

    void Start()
    {
        _gameManager = transform.parent.GetComponent<GameManager>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            other.gameObject.SetActive(false);
            _gameManager.DecrementResources(10);
        }
    }
}
