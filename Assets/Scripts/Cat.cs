﻿using UnityEngine;
using System.Collections;

public class Cat : MoveObject
{
    private GameManager _gameManager;

    public void Awake()
    {
        _gameManager = transform.parent.GetComponent<GameManager>();
    }

    public void OnMouseDown()
    {
        gameObject.SetActive(false);

        _gameManager.AddResources(10);
    }
}
