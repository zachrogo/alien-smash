﻿using UnityEngine;
using System.Collections;

public abstract class MoveObject : MonoBehaviour
{
    public class Coord
    {
        public float x;
        public float y;

        public Coord(float xCoord, float yCoord)
        {
            x = xCoord;
            y = yCoord;
        }
    }

    public float moveTime = .1f;
    private bool alive = true;
    private Vector3 target = new Vector3(0, -25, 0);
    private float totalDistance = 36.0f;
    private Coord currentCoord;
    private Coord targetCoord;

    protected virtual void Start()
    {
        currentCoord = new Coord(transform.position.x, transform.position.y);
        targetCoord = new Coord(GenerateX(), GenerateY());
    }

    protected virtual void FixedUpdate ()
    {
        if (IsAlive())
            ContinueMoving();
    }

    protected void ContinueMoving()
    {
        currentCoord = new Coord(transform.position.x, transform.position.y);

        if (ReachedTarget())
            GenerateNewTarget();

        Move();
    }

    protected void GenerateNewTarget()
    {
        targetCoord = new Coord(GenerateX(), GenerateY());
    }

    protected float GenerateX()
    {
        return Random.Range(-34.0f, 34.0f);
    }

    protected float GenerateY()
    {
        return Random.Range(-50, currentCoord.y);
    }

    protected void Move()
    {
        transform.position = Vector3.MoveTowards(CoordToVector3(currentCoord.x, currentCoord.y), CoordToVector3(targetCoord.x, targetCoord.y), moveTime);

        // increase speed over time
        moveTime += .0012f;
    }

    protected Vector3 CoordToVector3(float x, float y)
    {
        return new Vector3(x, y, 0);
    }

    protected bool ReachedTarget()
    {
        return CompareX() && CompareY();
    }

    protected bool CompareX()
    {
        return currentCoord.x == targetCoord.x;
    }

    protected bool CompareY()
    {
        return currentCoord.y == targetCoord.y;
    }

    protected void SetAlive(bool value)
    {
        alive = value;
    }

    protected bool IsAlive()
    {
        return alive;
    }
}