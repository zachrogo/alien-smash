﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour
{
    public GameObject alien;
    public GameObject builder;
    public GameObject wall;
    public GameObject cat;
    public Text resourcesText;
    public Text timerText;
    public Text gameOverText;
    public Text scoreText;

    private int totalResources = 100;
    private int score = 0;
    private int timeLeft = 120;
    private bool status;
    private float wallX = -35.0f;
    private Builder builderScript;

    // Use this for initialization
	protected void Start()
	{
	    builderScript = builder.GetComponent<Builder>();

	    Instantiate(alien, new Vector3(Random.Range(-35, 35), 19, 0), transform.rotation, transform);

	    InvokeRepeating("SetTimerText", 1.0f, 1.0f);
	    InvokeRepeating("AddWall", 12.0f, 12.0f);
	    InvokeRepeating("SpawnProcess", 3.0f, 3.0f);
	    InvokeRepeating("SpawnCats", 20.0f, 20.0f);
	}

    protected void Update()
    {
        SetResourceText();
        SetScoreText();
        if (isGameOver())
        {
            gameOverText.text = "You " + GetStatus() + "! Press enter to restart!";
            Time.timeScale = 0;
            if (Input.GetKeyDown("return"))
            {
                Application.LoadLevel("Main");
                Time.timeScale = 1;
            }
        }
    }

    public void IncreaseScore(int value)
    {
        score += value;
    }

    public void SetScoreText()
    {
        scoreText.text = "Score: " + score.ToString();
    }

    public void SetResourceText()
    {
        resourcesText.text = "Resources: " + totalResources.ToString();
    }

    public void SetTimerText()
    {
        timerText.text = "Timer: " + --timeLeft;
    }

    private string GetStatus()
    {
        return (status) ? "Win" : "Lost";
    }

    private bool isGameOver()
    {
        if (CheckIfWon())
        {
            status = true;
            return true;
        }
        else if (CheckIfLoss())
        {
            status = false;
            return true;
        }

        return false;
    }

    private bool CheckIfLoss()
    {
        return totalResources == 0;
    }

    private bool CheckIfWon()
    {
        return timeLeft == 0;
    }

    public void DecrementResources(int value)
    {
        totalResources -= value;
    }

    private void SpawnEnemy ()
	{
		Instantiate(alien, new Vector3(Random.Range(-30, 30), 19, 0), transform.rotation, transform);
	}

    private void SpawnCat()
    {
		Instantiate(cat, new Vector3(Random.Range(-30, 30), 19, 0), transform.rotation, transform);
    }

    private void SpawnProcess()
    {
        // number of aliens to spawn this wave
        int num = Random.Range(1, 10);

        for (int i = 0; i <= num; i++)
            SpawnEnemy();
    }

    private void SpawnCats()
    {
        // number of cats to spawn this wave
        int num = Random.Range(1, 6);

        for (int i = 0; i <= num; i++)
            SpawnCat();
    }

    public void AddResources(int value)
    {
        totalResources += value;
    }

    private void AddWall()
    {
        Instantiate(wall, new Vector3(wallX, -20, 0), transform.rotation);
        wallX += 7.5f;
        builderScript.MoveBuilder();
    }
}