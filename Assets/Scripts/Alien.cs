﻿using UnityEngine;
using System.Collections;

public class Alien : MoveObject
{
    private Animator animator;
    private SpriteRenderer sprite;
    private GameManager _gameManager;

    void Awake()
    {
        animator = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();
        _gameManager = transform.parent.GetComponent<GameManager>();
    }

    void OnMouseDown()
    {
        // Deactivate the movement of the sprite using parent member
        SetAlive(false);

        // Rescale the blood spatter sprite
        transform.localScale = new Vector3(1f, 1f, 0f);

        animator.Play("AlienDeath");

        gameObject.layer = 9;
        sprite.sortingLayerName = "Death";

        _gameManager.IncreaseScore(10);
    }
}
